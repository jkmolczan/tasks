"""
Quick sort algorithm.
"""


def sort(array, start, end):
    """
    :param array:
    :param start:
    :param end:
    :return:
    """
    left_marker = start
    for i in range(start, end):
        if array[i] <= array[end]:
            array[i], array[left_marker] = array[left_marker], array[i]
            left_marker += 1
    array[end], array[left_marker] = array[left_marker], array[end]
    return left_marker


def quick_sort(data, start, end):
    """
    :param data:
    :param start:
    :param end:
    :return:
    """
    if start < end:
        partition_index = sort(data, start, end)
        quick_sort(data, start, partition_index - 1)  # left partition
        quick_sort(data, partition_index + 1, end)  # right partition


def main():
    """
    :return:
    """
    data_to_sort = [9, 3, 5, 2, 8, 11, 6, 13, 14, 7]
    quick_sort(data_to_sort, 0, len(data_to_sort) - 1)
    print('Sorted list: {0}'.format(data_to_sort))


if __name__ == '__main__':
    main()
