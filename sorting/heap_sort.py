"""
Heap sort algorithm.
"""


class Heap:
    """
    class representing Heap.
    """
    def __init__(self, values):
        self.heap = values
        self.heap.insert(0, None)
        self._rebuild()

    def _rebuild(self):
        """
        :return:
        """
        for i in range(2, len(self.heap)):
            j, k = i, i // 2
            last_value = self.heap[i]
            while k > 0 and self.heap[k] < last_value:
                self.heap[j] = self.heap[k]
                j = k
                k = j // 2
            self.heap[j] = last_value

    def add(self, value):
        """
        :param value:
        :return:
        """
        self.heap.append(value)
        self._rebuild()

    def read_hip(self):
        """
        :return:
        """
        sorted_data = []
        for _ in range(1, len(self.heap)):
            sorted_data.insert(0, self.heap.pop(1))
            self._rebuild()
        return sorted_data


def main():
    """
    :return:
    """
    heap = Heap([17, 19, 4, 7, 17, 19, 18, 16, 10, 16])
    ordered_collection = heap.read_hip()
    print(ordered_collection)


if __name__ == '__main__':
    main()
