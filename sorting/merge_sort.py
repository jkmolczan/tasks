"""
Merge sort algorithm.
"""


def merge_sort(array, start, end):
    """
    :param array:
    :param start:
    :param end:
    :return:
    """
    if start != end:
        pointer = (start + end) // 2
        merge_sort(array, start, pointer)
        merge_sort(array, pointer + 1, end)
        merge(array, start, pointer, end)
    return array


def merge(data_series, start_pointer, center_pointer, end_pointer):
    """
    :param data_series:
    :param start_pointer:
    :param center_pointer:
    :param end_pointer:
    :return:
    """
    tmp = []
    i, j = start_pointer, center_pointer + 1
    while i <= center_pointer and j <= end_pointer:
        if data_series[i] < data_series[j]:
            tmp.append(data_series[i])
            i += 1
        else:
            tmp.append(data_series[j])
            j += 1

    if i <= center_pointer:
        while i <= center_pointer:
            tmp.append(data_series[i])
            i += 1
    else:
        while j <= end_pointer:
            tmp.append(data_series[j])
            j += 1

    elements_count = (end_pointer - start_pointer) + 1
    i = 0
    while i < elements_count:
        data_series[start_pointer + i] = tmp[i]
        i += 1


def main():
    """
    :return:
    """
    data = [17, 19, 4, 7, 17, 19, 18, 16, 10, 16]
    merge_sort(data, 0, len(data) - 1)
    print(data)


if __name__ == '__main__':
    main()
