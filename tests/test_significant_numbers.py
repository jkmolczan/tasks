"""
Tests significant numbers.
"""
import unittest
from algorithm_problems.significant_numbers import sig_in_interval, is_significant


class TestService(unittest.TestCase):
    """
    Class for tests.
    """

    def test_significant_numbers_in_interval(self):
        """
        :return:
        """
        numbers_interval = (4, 6)
        self.assertListEqual(
            sig_in_interval(numbers_interval),
            [4],
            'Significant numbers should be list with one element.'
        )

    def test_is_number_significant(self):
        """
        :return:
        """
        self.assertTrue(is_significant(4), 'Number should be significant.')

    def test_is_not_number_significant(self):
        """
        :return:
        """
        self.assertFalse(
            is_significant(6),
            'Number should be not significant.')


if __name__ == '__main__':
    unittest.main()
