"""
Tests.
"""
import unittest
from algorithm_problems.valuation_service import Service


class TestService(unittest.TestCase):
    """
    Tests for service.
    """

    def test_value_index_on_prices_list_should_be_on_the_top_of_list(self):
        """
        :return:
        """
        service = Service()
        fake_list = [20, 11, 6, 3]
        value = 33
        count = 5
        value_index = service.value_index_on_prices_list(
            value, fake_list, count)
        self.assertIs(
            value_index,
            0,
            'Value index should be on the top of list')

    def test_value_index_on_prices_list_should_return_zero(self):
        """
        :return:
        """
        service = Service()
        fake_list = []
        value = 33
        count = 5
        value_index = service.value_index_on_prices_list(
            value, fake_list, count)
        self.assertIs(value_index, 0, 'Should return zero')

    def test_value_index_on_prices_list_should_return_none(self):
        """
        :return:
        """
        service = Service()
        fake_list = [44]
        value = 33
        count = 1
        value_index = service.value_index_on_prices_list(
            value, fake_list, count)
        self.assertIsNone(value_index, 'Should be none')

    def test_get_currencies_should_return_undefined_as_target_currency(self):
        """
        :return:
        """
        service = Service()
        currencies_ratios, target_currency = service.get_currencies_ratios(
            'tests/fake_currencies.csv')
        self.assertIsInstance(
            currencies_ratios,
            dict,
            'Currencies ratios should be a dict')
        self.assertIs(target_currency, 'Undefined')

    def test_get_currencies_should_return_file_not_found_error(self):
        """
        :return:
        """
        service = Service()
        with self.assertRaises(FileNotFoundError):
            service.get_currencies_ratios('curr.csv')

    def test_calculate_prices(self):
        """
        :return:
        """
        service = Service()
        fake_ratios = {'GBP': 2.4, 'PLN': 1.0, 'EU': 2.1}
        fake_products = [
            {'price': '1000', 'quantity': '2', 'matching_id': '3', 'id': '1', 'currency': 'GBP'},
            {'price': '1050', 'quantity': '1', 'matching_id': '2', 'id': '2', 'currency': 'EU'},
            {'price': '2000', 'quantity': '1', 'matching_id': '1', 'id': '3', 'currency': 'PLN'},
            {'price': '1750', 'quantity': '2', 'matching_id': '2', 'id': '4', 'currency': 'EU'},
            {'price': '1400', 'quantity': '4', 'matching_id': '3', 'id': '5', 'currency': 'EU'},
            {'price': '7000', 'quantity': '3', 'matching_id': '2', 'id': '6', 'currency': 'PLN'},
            {'price': '1400', 'quantity': '3', 'matching_id': '2', 'id': '9', 'currency': 'GBP'},
            {'price': '2000', 'quantity': '1', 'matching_id': '1', 'id': '7', 'currency': 'GBP'}]
        fake_top_prices = {'1': 0, '2': 3, '3': 2}
        prices = service.calculate_prices(
            fake_ratios, fake_products, fake_top_prices)
        self.assertIsInstance(prices, dict)
        self.assertDictEqual(
            {'prices': [21000.0, 10080.0, 7350.0], 'ignored_products_count': 1}, prices['2'])
        self.assertDictEqual(
            {'prices': [], 'ignored_products_count': 2}, prices['1'])


if __name__ == '__main__':
    unittest.main()
