"""
A zero-indexed array A consisting of N different integers is given.
The array contains integers in the range [1..(N + 1)],
which means that exactly one element is missing.

Your goal is to find that missing element.

def solution(A)
that, given a zero-indexed array A, returns the value of the missing element.

For example, given array A such that:
  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5
the function should return 4, as it is the missing element.
"""


def solution(arr):
    """

    :param arr:
    :return:
    """
    if not arr:
        return 1
    if len(arr) == 1:
        # guarantees the correctness of data when first value in array is 1
        return arr[0] + 1
    arr.sort()
    count = len(arr)
    if arr[0] != 1:  # case when array isn't correct
        return 1
    for index, value in enumerate(arr):
        if index + 1 < count and arr[index + 1] - value != 1:
            return arr[index + 1] - 1
    return arr[-1] + 1
