"""
Given a collection of intervals, merge all overlapping intervals.
    For example,
    Given [1,3],[2,6],[8,10],[15,18],
    return [1,6],[8,10],[15,18].
    you can assume y > x for each interval [x, y]
"""


def merge_intervals(*args):
    """
    :param args:
    :return:
    """
    stack = []
    intervals = list(args)
    stack.append(intervals.pop(0))
    for interval in intervals:
        if interval[0] > stack[len(stack) - 1][-1]:
            stack.append(interval)
        else:
            stack[len(stack) - 1][-1] = interval[-1]
    return stack


if __name__ == '__main__':
    print(merge_intervals([1, 3], [2, 6], [8, 10], [15, 18], [16, 22]))
