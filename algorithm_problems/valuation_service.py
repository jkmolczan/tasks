"""
On the input you've got 3 files containing:
* data.csv - product representation with price,currency,quantity,matching_id
* currencies.csv - currency code and ratio to PLN,
ie. GBP,2.4 can be converted to PLN with procedure 1 PLN * 2.4
* matchings.csv - matching data matching_id,top_priced_count

Read all the data. From products with particular matching_id take those with the
highest total price(price * quantity),
limit data set by top_priced_count and aggregate prices.
Result save to top_products.csv with four columns:
matching_id,total_price,avg_price,currency, ignored_products_count.

"""

import logging

LOGGER = logging.getLogger(__name__)


class Service:
    """
    class representing service
    """
    @staticmethod
    def value_index_on_prices_list(value, prices_list, max_list_count):
        """
        :param value:
        :param prices_list:
        :param max_list_count:
        :return:
        """
        if prices_list:
            index_on_prices_list = None
            for index, price in enumerate(prices_list):
                if value > price:
                    return index
            if len(prices_list) < max_list_count:
                index_on_prices_list = len(prices_list)
            return index_on_prices_list
        return 0

    @staticmethod
    def get_currencies_ratios(file_path):
        """
        :param file_path:
        :return:
        """
        ratios = {}
        target_currency = 'Undefined'
        with open(file_path, 'r') as currencies:
            next(currencies)
            for line in currencies:
                currency, ratio = line.strip().split(',')
                if float(ratio) == 1.0:
                    target_currency = currency
                ratios[currency] = float(ratio)
        return ratios, target_currency

    @staticmethod
    def get_products_data(file_path):
        """
        :param file_path:
        :return:
        """
        products = []
        with open(file_path, 'r') as products_data_file:
            products_head = next(products_data_file).strip().split(',')
            for line in products_data_file:
                values = line.strip().split(',')
                product_data = {}
                for num, item in enumerate(products_head):
                    product_data[item] = values[num]
                products.append(product_data)
        return products

    @staticmethod
    def get_top_prices_count_for_product_id(file_path):
        """
        :param file_path:
        :return:
        """
        top_prices_products_count = {}
        with open(file_path, 'r') as top_prices_matchings:
            next(top_prices_matchings)
            for line in top_prices_matchings:
                matching_id, prices_count = line.strip().split(',')
                top_prices_products_count[matching_id] = int(prices_count)
        return top_prices_products_count

    def calculate_prices(self, ratios, products, top_prices_count):
        """
        :param ratios:
        :param products:
        :param top_prices_count:
        :return:
        """
        top_products_data = {}
        for item in top_prices_count.keys():
            top_products_data[item] = {
                'prices': [], 'ignored_products_count': 0}
        for product in products:
            if ratios.get(product['currency']):
                total_product_price = float(ratios[product['currency']]) * \
                                      float(product['price']) * \
                                      float(product['quantity'])
                product_id = product['matching_id']
                append_index = self.value_index_on_prices_list(
                    total_product_price,
                    top_products_data[product_id]['prices'],
                    top_prices_count[product_id])
                if append_index is not None:
                    top_products_data[product_id]['prices'].insert(
                        append_index, total_product_price)
                    if len(
                            top_products_data[product_id]['prices']) > top_prices_count[product_id]:
                        top_products_data[product_id]['prices'].pop()
                        top_products_data[product_id]['ignored_products_count'] += 1
            else:
                LOGGER.info('Unhandled currency, skip.')
        return top_products_data

    @staticmethod
    def save_top_products(file_path, top_products_data, price_currency):
        """
        :param file_path:
        :param top_products_data:
        :param price_currency:
        :return:
        """
        with open(file_path, 'w') as top_products_file:
            head = 'matching_id,total_price,avg_price,currency, ignored_products_count\n'
            top_products_file.write(head)
            for matched_id, top_product_data in top_products_data.items():
                total_price = sum(top_product_data['prices'])
                avg_price = total_price / len(top_product_data['prices'])
                ignored_products_count = top_product_data['ignored_products_count']
                data = '{},{},{},{},{}\n'.format(
                    matched_id,
                    total_price,
                    avg_price,
                    price_currency,
                    ignored_products_count)
                top_products_file.write(data)


def main():
    """
    :return:
    """
    service = Service()
    currencies_ratios, prices_currency = service.get_currencies_ratios(
        'currencies.csv')
    products_data = service.get_products_data('data.csv')
    top_prices_count_for_product = service.get_top_prices_count_for_product_id(
        'mathings.csv')
    top_products = service.calculate_prices(
        currencies_ratios,
        products_data,
        top_prices_count_for_product)
    service.save_top_products(
        'top_products.csv',
        top_products,
        prices_currency)


if __name__ == '__main__':
    main()
