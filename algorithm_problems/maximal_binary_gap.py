"""
    Maximal count of zero on binary representation of number
    ex. 1010001 max binary gap is 3.
"""


def maximal_binary_gap(word):
    """
    :param word:
    :return:
    """
    max_counter = 0
    counter = 0
    count = False
    word = bin(word)
    for i in word[2:]:
        if int(i) == 1:
            if count:
                if max_counter < counter:
                    max_counter = counter
                counter = 0
            count = True
        elif count:
            counter += 1
    return max_counter


if __name__ == '__main__':
    MAX_GAP = maximal_binary_gap(1041)
    print(MAX_GAP)
