"""
Write a program that prints the integers from n to m (inclusive).
Requirements
*	for multiples of three, print Fizz (instead of the number)
*	for multiples of five, print Buzz (instead of the number)
*	for multiples of both three and five, print FizzBuzz (instead of the number)

1 <= n < m <= 10000

Example:
Sample input:
3
16

Sample output:
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
"""

import sys


def generate_inscriptions(start_range, end_range):
    """

    :param start_range:
    :param end_range:
    :return:
    """
    while start_range <= end_range:
        def get_inscription():
            if start_range % 3 and start_range % 5:
                return start_range
            if start_range % 3 == 0:
                if ((start_range / 3) % 5) == 0:
                    return 'FizzBuzz'
                return 'Fizz'
            return 'Buzz'
        yield get_inscription()
        start_range += 1


if __name__ == '__main__':

    print('Please input two integers between 1 and 1000 in two lines, ex.\n3\n16')
    try:
        START = int(input())
        END = int(input())
    except ValueError as err:
        print('This is not a number.')
        sys.exit(0)

    if START < 1 or END > 10000:
        print('Numbers are not from the specified range.')
        sys.exit(0)

    for inscription in generate_inscriptions(START, END):
        print(inscription)
