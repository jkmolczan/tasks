"""
    Returns significant numbers from the given interval.
    Significant number - average of all divisors of this number (excluding 1 and given number)
    does not bigger than the sqr of this number.
    Example: 9 is significant number because average all divisors is 3 and sqr of 9 is 3.
    Exercise from: https://pl.spoj.com/problems/PZPI3/
"""


def sig_in_interval(numbers_interval):
    """
    :param numbers_interval: tuple with boundary conditions of interval.
    :return: list with significant numbers interval.
    """
    start_interval, end_interval = numbers_interval
    return [
        i for i in range(
            start_interval,
            end_interval) if is_significant(i)]


def is_significant(number):
    """
    :param number:
    :return:
    """
    divisors_list = [i for i in range(2, number) if number % i == 0]
    if not divisors_list:
        return False
    divisors_average = sum(divisors_list) / len(divisors_list)
    return divisors_average <= number**(1 / 2)
